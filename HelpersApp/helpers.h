#pragma once

int sumSqrt(int firstNum, int secondNum) {
	return (firstNum * firstNum + secondNum * secondNum);
}